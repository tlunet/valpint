# Validation Test Cases for Parallel-in-Time Algorithm Implementations

This project intend to list some parallel-in-time algorithms and describe a few validation test cases that can be used to validate their implementations.

## Structure

... incomming

## List of algorithms

- MultiGrid Reduction In Time ([MGRIT](./mgrit)), from [S. Friedhoff, R.D Falgout, T.V Kolev, S. MacLachlan and J.B Schroder](https://www.osti.gov/biblio/1073108)
- [Parareal](./parareal), from [J.L Lions. Y. Maday and G. Turinici](https://www.sciencedirect.com/science/article/pii/S0764444200017936?via%3Dihub)
