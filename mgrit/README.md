# Validation test cases for MGRIT

## [ADV](./adv)

Compute the solution of the 1D linear advection equation:
```math
\frac{\partial u}{\partial t} + \frac{\partial u}{\partial x} = 0
```
with $`x`$ in the periodic domain [-1, 1], and $`t \in [0, 1]`$.

**Baseline configuration**

* Initial condition: $`u(x,0) = \cos(\pi x)`$
* Initial guess: $`u^0(x,t) = 0`$
* Space grid: uniform regular mesh $`[x_1, ..., x_{16}]`$, with $`\delta_x=0.125`$, $`x_1=-1`$ and $`x_{16}=1-\delta_x`$
* Space derivative: First order Upwind
```math
\frac{\partial u}{\partial x} \simeq \frac{u_i-u_{i-1}}{\delta_x}
```
* Time stepping: uniform regular mesh $`[t_0, ..., t_{20}]`$, with $`\delta_t=0.05`$, $`t_0=0`$ and $`t_{20}=1`$
* Time integration: Forward Euler (explicit)
```math
u^{n+1} = u^{n} + \delta_t f(u^{n})
```

**Test data:** ASCII file (number format `%20.18f`) containing
```math
u(t_0, x_0)\\
u(t_{1}, x_0)\\
u(t_{2}, x_0)\\
u(t_{3}, x_0)\\
u(t_{4}, x_0)\\
u(t_{5}, x_0)\\
u(t_{6}, x_0)\\
u(t_{7}, x_0)\\
u(t_{8}, x_0)\\
u(t_{9}, x_0)\\
u(t_{10}, x_0)
```

### [Test-01](./adv/test-01)

* Number of levels: 2
* Coarsening factor: 2
* Relaxation type: F
* Number of iterations: 2

### [Test-02](./adv/test-02)

* Number of levels: 2
* Coarsening factor: 4
* Relaxation type: F
* Number of iterations: 2

### [Test-03](./adv/test-03)

* Number of levels: 2
* Coarsening factor: 2
* Relaxation type: FCF
* Number of iterations: 2

### [Test-04](./adv/test-04)

* Number of levels: 3
* Coarsening factor: 2
* Relaxation type: F
* Number of iterations: 2
* Cycle type: F

### [Test-05](./adv/test-05)

* Number of levels: 3
* Coarsening factor: 2
* Relaxation type: F
* Number of iterations: 2
* Cycle type: V
